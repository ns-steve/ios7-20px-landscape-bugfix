/* Steve@northstreetcreative.com
 * Version: 0.1, 12.17.2013
 * 
 * Quick & Dirty attempt to test for presence of the 20px ios7 landscape bug
 *
 * This code manually fixes the html height. As an alternative, you can uncomment
 * the removeClass/addClass lines and rely on css classes for your fixes instead */

jQuery(document).ready(function($) {

	// Check if it's ios7
	if (navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
		
		doOnOrientationChange();	
		
		jQuery(window).bind('orientationchange', doOnOrientationChange);
	
			function doOnOrientationChange()
			{
				var windowHeight = window.innerHeight;
				var htmlHeight   = $('html').height();
		
				switch(window.orientation)
				{
					case 0:
					case 180:
						// Portrait works fine
						//$('body').removeClass('ios7bug');
						$('html').css('height','');
					break;
					case 90:
					case -90:
						// is ios7 still buggy?
						if (windowHeight != htmlHeight) {
							// 'fraid so, let's manually set the height instead
							//$('body').addClass('ios7bug');
							newHeight = htmlHeight-19; // why 19 instead of 20? Rounding error
							window.scrollTo(0,0); 
							$('html').css('height',newHeight);
						}
					break;
				}
			} // end doOnOrientationChange()
	
	}
});